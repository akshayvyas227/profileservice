package com.swiggy.profile.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.swiggy.profile.models.Address;
import com.swiggy.profile.models.Profile;
import com.swiggy.profile.services.ProfileService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.util.Arrays;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ProfileController.class)
class ProfileControllerTest {

    private static ObjectMapper objectMapper = new ObjectMapper();

    private String toJsonString(Object object) throws JsonProcessingException {
        return objectMapper.writeValueAsString(object);
    }

    private Object toJavaObject(String jsonString) throws IOException {
        return objectMapper.readValue(jsonString, Profile.class);
    }

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProfileService profileService;

    @Test
    void testGetAllProfiles() throws Exception {
        when(profileService.getAllProfiles())
                .thenReturn(
                        Arrays.asList(
                                new Profile("akshay","98548"),
                                new Profile("kashaf","842543")
                        )
                );
        mockMvc.perform(
                get("/profiles")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", Matchers.hasSize(2)));

        verify(profileService).getAllProfiles();
    }

    @Test
    public void testToGetProfile() throws Exception {
        Profile profile = new Profile("akshay","97363456");

        String getProfileUrl = "/profiles/1";
        when(profileService.getProfile(1L)).thenReturn(profile);
        mockMvc.perform(get(getProfileUrl))
                .andExpect(status().isOk())
                .andExpect(content().json(toJsonString(profile)));

        verify(profileService).getProfile(1L);
    }

    @Test
    public void testToAddProfile() throws Exception {
        Profile profile = new Profile("akshay","876893457689");

        String addProfileUrl = "/profiles";
        when(profileService.addProfile(profile)).thenReturn(profile);

        String json = objectMapper.writeValueAsString(profile);
        mockMvc.perform(post(addProfileUrl)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"firstName\":\"akshay\", \"mobileNumber\":\"876893457689\"}"))
                .andExpect(status().isCreated())
                .andReturn();

        verify(profileService).addProfile(profile);
    }

    @Test
    public void expect400() throws Exception {
        String addProfileUrl = "/profiles";
        mockMvc.perform(post(addProfileUrl)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"firstName\":\"\", \"mobileNumber\":\"876893457689\"}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testToAddAddress() throws Exception {
        Profile profile = new Profile("akshay","876893457689");

        Address address = new Address("india","rajasthan","jodhpur","42335","5432543",profile);
        String addAddressUrl = "/profiles/1/addresses";
        when(profileService.addAddress(address)).thenReturn(address);

        String json = objectMapper.writeValueAsString(address);
        mockMvc.perform(post(addAddressUrl)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"id\": 1,\n" +
                        "    \"country\": \"india\",\n" +
                        "    \"state\": \"rajasthan\",\n" +
                        "    \"city\": \"jodhpur\",\n" +
                        "    \"addressLine\": \"42335\",\n" +
                        "    \"postalCode\": \"5432543\"\n" +
                        "}"))
                .andExpect(status().isCreated())
                .andReturn();

        verify(profileService).addAddress(any(Address.class));
    }
    @Test
    void testPartialUpdateProfileRoute() throws Exception {

        String requestBody = "{\n" +
                "  \"mobileNumber\": \"987589485\",\n" +
                "  \"firstName\": \"John\"\n" +
                "}";

        mockMvc.perform(
                patch("/profiles/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isOk());
        verify(profileService).updateProfile(eq(1L), anyMap());
    }

}
