package com.swiggy.profile.services;

import com.swiggy.profile.models.Profile;
import com.swiggy.profile.repository.ProfileRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class ProfileServiceTest {

    @Autowired
    private ProfileRepository profileRepository;

    @Autowired
    private ProfileService profileService;

    @AfterEach
    void clearRepositories() {
        profileRepository.deleteAll();
    }

    @Test
    void addProfileTest() {
        Profile profile = profileService.addProfile(new Profile("akshay","9685496845"));

        assertEquals(1, profileRepository.count());
        assertEquals(profileRepository.findById(profile.getId()).orElse(null), profile);
    }


    @Test
    void expectAllProfilesToBeFetched() {
        Profile profile = profileService.addProfile(new Profile("akshay","9685496845"));
        Profile otherProfile = profileService.addProfile(new Profile("kashaf","9895565"));

        assertEquals(2, profileRepository.count());
        assertEquals(2, profileRepository.count());
        assertEquals(Arrays.asList(profile, otherProfile), profileService.getAllProfiles());
    }

}