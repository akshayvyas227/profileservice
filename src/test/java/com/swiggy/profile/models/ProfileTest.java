package com.swiggy.profile.models;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ProfileTest {
    @Autowired
    private ObjectMapper objectMapper;

    private String toJsonString(Object object) throws JsonProcessingException {
        return objectMapper.writeValueAsString(object);
    }

    private Object toJavaObject(String jsonString) throws IOException {
        return objectMapper.readValue(jsonString, Profile.class);
    }

    @Test
      void testSerializationOfProfile() throws JsonProcessingException {

        Profile profile = new Profile("akshay","8756859349");
        String json = objectMapper.writeValueAsString(profile);

     assertEquals("{\"id\":null,\"firstName\":\"akshay\",\"mobileNumber\":\"8756859349\",\"emailId\":null,\"lastName\":null,\"addresses\":[]}", json);
  }

   @Test
    void testDeserializationOfProfile() throws IOException {
        String json = "{\"id\":null,\"firstName\":\"akshay\",\"mobileNumber\":\"8756859349\",\"emailId\":null,\"lastName\":null,\"addresses\":[]}";
        Profile profile = objectMapper.readValue(json, Profile.class);
        assertEquals(new Profile("akshay","8756859349"),profile);
    }
}