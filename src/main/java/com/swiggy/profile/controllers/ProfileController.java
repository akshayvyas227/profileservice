package com.swiggy.profile.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.swiggy.profile.models.Address;
import com.swiggy.profile.models.Profile;
import com.swiggy.profile.services.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
public class ProfileController {

    @Autowired
    private ProfileService profileService;

    @Autowired
    private ObjectMapper objectMapper;

    @GetMapping("/profiles")
    public List<Profile> getAllProfiles() {
        return profileService.getAllProfiles();
    }

    @GetMapping("/profiles/{id}")
    public Profile getProfile(@PathVariable Long id) {
        return profileService.getProfile(id);
    }

    @PostMapping(path = "/profiles")
    @ResponseStatus(HttpStatus.CREATED)
    public Profile addProfile(@Valid @RequestBody Profile profile) {
        return profileService.addProfile(profile);
    }

    @PostMapping(path = "/profiles/{profileId}/addresses")
    @ResponseStatus(HttpStatus.CREATED)
    public Address addAddress(@Valid @RequestBody Address address, @PathVariable Long profileId) {
        address.setProfile(profileService.getProfile(profileId));
        return profileService.addAddress(address);
    }

    @PatchMapping(path = "/profiles/{id}")
    public Profile updateProfile(@PathVariable Long id, @RequestBody Map<String, Object> fields) {
        return profileService.updateProfile(id,fields);
    }

}