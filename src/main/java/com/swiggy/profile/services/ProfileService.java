package com.swiggy.profile.services;

import com.swiggy.profile.models.Address;
import com.swiggy.profile.models.Profile;
import com.swiggy.profile.repository.AddressRepository;
import com.swiggy.profile.repository.ProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.lang.reflect.Field;
import java.util.*;

@Service
public class ProfileService {

    @Autowired
    private ProfileRepository profileRepository;

    @Autowired
    private AddressRepository addressRepository;


    @Autowired
    public ProfileService(ProfileRepository profileRepository) {
        this.profileRepository = profileRepository;
    }

    public List<Profile> getAllProfiles() {
        List<Profile> profiles = new ArrayList<>();
        profileRepository.findAll().forEach(profiles::add);
        return profiles;
    }

    public Optional<Profile> fetchProfile(Long id) {
        return profileRepository.findById(id);
    }

    public Profile getProfile(Long id) {
        Optional<Profile> profile = fetchProfile(id);
        if (profile.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "profile not found");
        }
        return profile.get();
    }

    public Profile addProfile(Profile profile) {
        Long id = profile.getId();

        if (id!=null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "profile already exists");
        }
        return profileRepository.save(profile);
    }

    public Profile updateProfile(Long id, Map<String, Object> fields) {
        if (profileRepository.findById(id).isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "requested profile does not exist");
        }

        Profile profile = getProfile(id);
        fields.forEach((k, v) -> {
            Field field = ReflectionUtils.findField(Profile.class, k);
            field.setAccessible(true);
            ReflectionUtils.setField(field, profile, v);
            field.setAccessible(false);
        });

        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        Validator validator = validatorFactory.getValidator();
        Set<ConstraintViolation<Profile>> violations =  validator.validate(profile);
        if (!violations.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Request body is not valid.");
        }

        return profileRepository.save(profile);
    }

    public Address addAddress(Address address) {
        Long id = address.getId();

        if (id!=null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "address already exists");
        }
        return addressRepository.save(address);
    }

}
