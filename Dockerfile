FROM openjdk:12.0.1
VOLUME /tmp
COPY build/libs/profile-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
